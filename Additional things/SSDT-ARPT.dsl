DefinitionBlock ("", "SSDT", 2, "ARDARA", "ARPT", 0x00000000)
{
    External (_SB_.PCI0.RP07, DeviceObj)    // (from opcode)
    External (_SB_.PCI0.RP07.PXSX, DeviceObj)    // (from opcode)
    External (DTGP, MethodObj)    // 5 Arguments (from opcode)
    External (PXSX, DeviceObj)    // (from opcode)

    Scope (\_SB.PCI0.RP07)
    {
        Scope (PXSX)
        {
            Name (_STA, Zero)  // _STA: Status
        }

        Device (ARPT)
        {
            Name (_ADR, Zero)  // _ADR: Address
            Name (_PRW, Package (0x02)  // _PRW: Power Resources for Wake
            {
                0x09, 
                0x04
            })
            Method (_DSM, 4, NotSerialized)  // _DSM: Device-Specific Method
            {
                Store (Package ()
                    {
                        "AAPL,slot-name", "AirPort", 
                        "name", "AirPort Extreme", 
                        "device-id", Buffer() { 0xb1, 0x43, 0x00, 0x00 },
                        "ventor-id", Buffer() { 0xe4, 0x14, 0x00, 0x00 },
                        "subsystem-ventor-id", Buffer() { 0x6b, 0x10, 0x00, 0x00 },
                        "subsystem-id", Buffer() { 0x19, 0x00, 0x00, 0x00 },
                        "model", "Broadcom BCM4322x 802.11 a/b/g/n Wireless Network Controller", 
                        "device_type", Buffer () { "AirPort" }, 
                        "compatible", "pci14e4,43a0", 
                        "built-in", Buffer () { 0x01 }
                    }, Local0)
                DTGP (Arg0, Arg1, Arg2, Arg3, RefOf (Local0))
                Return (Local0)
            }
        }
    }
}

