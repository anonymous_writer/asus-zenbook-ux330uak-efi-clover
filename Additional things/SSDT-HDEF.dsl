DefinitionBlock ("", "SSDT", 2, "HACK", "AUDIO", 0x00000000)
{
    External (_SB_.PCI0.HDAS, DeviceObj)    // (from opcode)

    Scope (\_SB.PCI0.HDAS)
    {
        Method (_DSM, 4, NotSerialized)  // _DSM: Device-Specific Method
        {
            If (LEqual (Arg2, Zero))
            {
                Return (Buffer ()
                {
                     0x03                                           
                })
            }

            Return (Package ()
            {
                "alc-layout-id", Buffer () {0x017, 0x00, 0x00, 0x00}, 
                "layout-id", Buffer () {0x11, 0x00, 0x00, 0x00}, 
                "hda-gfx", Buffer () {"onboard-1"}, 
                "model", Buffer () {"Sunrise Point-LP HD Audio"}, 
                "name", Buffer () {"Realtek ALC255"}, 
                "MaximumBootBeepVolume", Buffer () {0xEF}, 
                "MaximumBootBeepVolumeAlt", Buffer () {0xF1}, 
                "multiEQDevicePresence", Buffer () {0x0C, 0x00, 0x01, 0x00},            
                "AAPL,slot-name", Buffer () {"Built In"}, 
                "built-in", Buffer () {"0x00"}, 
                "device_type", Buffer () {"High Definition Audio"},
                "PinConfigurations", Buffer () {"0x00"},
            })
        }
    }
}

